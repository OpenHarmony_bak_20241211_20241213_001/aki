## Example 10: *map_for_object*

环境依赖：
* IDE： DevEco Studio 4.0 Beta2
* SDK：4.0.9.6
* IDE： DevEco Studio 3.1.0.500
* SDK：3.2.12.2

### 1. 依赖配置

- 本例使用源码依赖

    ```bash
    cd entry/src/main/cpp
    git clone
    ```

    CMakeLists.txt 配置依赖
    ```cmake
    add_subdirectory(aki)
    target_link_libraries(entry PUBLIC aki_jsbind)
    ```

### 2. std::map<std::string, long>传值用例说明


`index.d.ts`:
```js

export const transferMapReturnMap: (map:object) => object;

```

`index.ets`:
```js
  transferMapReturnMap(){
    let result=libmap_for_object.transferMapReturnMap({
      'apple':1,
      'orange':2,
      "banana": 3
    });
    console.log('[AKI]',"transferMapReturnMap:"+JSON.stringify(result));
  }
```

`C++`：

```C
std::map<std::string, long> TransferMapReturnMap(std::map<std::string, long> map) {
    AKI_DLOG(DEBUG) << "TransferMapReturnMap apple:" << map["apple"];
    AKI_DLOG(DEBUG) << "TransferMapReturnMap banana:" << map["banana"];
    AKI_DLOG(DEBUG) << "TransferMapReturnMap orange:" << map["orange"];
    return map;
}
```


