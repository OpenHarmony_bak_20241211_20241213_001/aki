## Example 20: *structure_field*

环境依赖：
* IDE： DevEco Studio 4.0 Beta2
* SDK：4.0.9.6
* IDE： DevEco Studio 3.1.0.500
* SDK：3.2.12.2

### 1. 依赖配置

- 本例使用源码依赖

    ```bash
    cd entry/src/main/cpp
    git clone
    ```

  CMakeLists.txt 配置依赖
    ```cmake
    add_subdirectory(aki)
    target_link_libraries(entry PUBLIC aki_jsbind)
    ```

### 2. 用例说明


`index.d.ts`:
```js
export class Person{
    name: string;
    constructor(name: string)
}

export const foo: (bean: object) => Person;
```

`index.ets`:
```js
      Text("JSBIND_FIELD_SET_GET")
          .fontSize(50)
          .fontWeight(FontWeight.Bold)
          .onClick(() => {
            let person = new libAddon.Person("jsbind");
            let other = libAddon.foo(person);
            hilog.info(0x0000, 'AKI', 'Person name = %{public}s', other.name);
          })

```


`C++`：

```C
struct Person {
    // 构造函数，用于JS侧 new 对象
    Person(std::string name) : name(name){}
    
    // Get 用于做属性访问，JS侧无需访问类对象，可不定义
    std::string GetName() {
        return name;
    }
    
    // Set 用于做属性访问，JS侧无需访问类对象，可不定义
    void SetName(std::string name) {
        this->name = name;
    }
    
    std::string name;
};

JSBIND_CLASS(Person) {
    JSBIND_CONSTRUCTOR<std::string>(); // 绑定构造函数
    JSBIND_FIELD_SET("name", SetName); // 单个设置Set
    JSBIND_FIELD_GET("name", GetName); // 单个设置Get
}

Person foo(Person person) {
    person.name += " for Ark";
    return person;
}

JSBIND_GLOBAL() {
    JSBIND_FUNCTION(foo);
}
```