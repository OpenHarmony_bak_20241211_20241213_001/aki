## Example 11: *napi_value*

环境依赖：
* IDE： DevEco Studio 4.0 Beta2
* SDK：4.0.9.6
* IDE： DevEco Studio 3.1.0.500
* SDK：3.2.12.2

### 1. 依赖配置

- 本例使用源码依赖

    ```bash
    cd entry/src/main/cpp
    git clone
    ```

    CMakeLists.txt 配置依赖
    ```cmake
    add_subdirectory(aki)
    target_link_libraries(entry PUBLIC aki_jsbind)
    ```

### 2. napi_value传值用例说明


`index.d.ts`:
```js

import image from '@ohos.multimedia.image';

export const getImagePixMap:(pixmap:image.PixelMap,func:(value:image.PixelMap)=>void) => image.PixelMap;
```

`index.ets`:

```js

import libraw_file from 'libraw_file.so'

 async getPixMap() {
    const fileData: Uint8Array = await getContext(this).resourceManager.getRawFileContent('icon.png');
    const imageSource = image.createImageSource(fileData.buffer)
    let pixmap = await imageSource.createPixelMap()
    let that = this;
    libraw_file.getImagePixMap(pixmap,(result)=>{
       that.imagePixelMap=result
    })
    imageSource.release()
  }
```

`C++`：

```C
napi_value GetImagePixMap(napi_value pixmap,aki::Callback<napi_value (napi_value)> callback) {
    OHOS::Media::OhosPixelMapInfo info;
    napi_env env = aki::JSBind::GetScopedEnv();
    OHOS::Media::OH_GetImageInfo(env, pixmap, &info);
    AKI_DLOG(DEBUG) << "GetImagePixMap-->" << info.height << info.width << info.pixelFormat;
    return callback(pixmap);
}
```


