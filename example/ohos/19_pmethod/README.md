## Example 19: *pmethod*

环境依赖：
* IDE： DevEco Studio 4.0 Beta2
* SDK：4.0.9.6
* IDE： DevEco Studio 3.1.0.500
* SDK：3.2.12.2

### 1. 依赖配置

- 本例使用源码依赖

    ```bash
    cd entry/src/main/cpp
    git clone
    ```

    CMakeLists.txt 配置依赖
    ```cmake
    add_subdirectory(aki)
    target_link_libraries(entry PUBLIC aki_jsbind)
    ```

### 2. JSBIND_PMETHOD用例说明


`index.d.ts`:
```js

export class TaskRunnerData {
  name: string;
}

export class TaskRunner {

  DoTaskParamObjReturnStruct: (bean: TaskRunnerData) => Promise<TaskRunnerData>;
}

```

`index.ets`:
```js
        Text("PMETHOD传对象")
          .fontSize(50)
          .fontWeight(FontWeight.Bold)
          .onClick(() => {
            let taskRunner = new libAddon.TaskRunner();
            let taskRunnerData = new libAddon.TaskRunnerData();
            taskRunnerData.name="TEST"
            taskRunner.DoTaskParamObjReturnStruct(taskRunnerData).then(res => {
              console.log('[AKI] DoTaskParamObjReturnStruct: ' +res.name)
            });
          })

```

`C++`：

```C
#include <aki/jsbind.h>


struct TaskRunnerData {
    TaskRunnerData() = default;
    TaskRunnerData(std::string name) : name(name) {}
    std::string name;
};

JSBIND_CLASS(TaskRunnerData) {
    JSBIND_CONSTRUCTOR<>();
    JSBIND_PROPERTY(name);
}

class TaskRunner {
public:
    TaskRunner() = default;

    TaskRunnerData DoTaskParamObjReturnStruct(TaskRunnerData obj) {
        AKI_LOG(INFO) << "DoTaskParamObjReturnStruct";
        return obj;
    }
};

JSBIND_CLASS(TaskRunner) {
    JSBIND_CONSTRUCTOR<>();
    JSBIND_PMETHOD(DoTaskParamObjReturnStruct);
}

JSBIND_ADDON(pmethod);
```


