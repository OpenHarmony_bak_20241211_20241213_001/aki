## Example 15: *aki_value*

环境依赖：
* IDE： DevEco Studio 4.0 Beta2
* SDK：4.0.9.6
* IDE： DevEco Studio 3.1.0.500
* SDK：3.2.12.2

### 1. 依赖配置

- 本例使用源码依赖

    ```bash
    cd entry/src/main/cpp
    git clone
    ```

    CMakeLists.txt 配置依赖
    ```cmake
    add_subdirectory(aki)
    target_link_libraries(entry PUBLIC aki_jsbind)
    ```

### 2. native调用JS类的方法用例说明


`index.d.ts`:
```js

export const PassingValueGetStudentName:(param:object) => string;

export const PassingValueGetStudentYear:(param:object) => number;

```

`StudentManager.ts`:
```js
export default class StudentManager {

  constructor() {
  }

  static getStudentName(): string {
    return "zhangsan"
  }

  getStudentYear(): number {
    return 19
  }
}
```

`index.ets`:
```js
  getStudentName() {
   let result = libaki.PassingValueGetStudentName(StudentManager);
    console.log("PassingValueGetStudentName:"+result)
  }

  getStudentYear() {
    let studentManager=new StudentManager();
    let result =libaki.PassingValueGetStudentYear(studentManager);
    console.log("PassingValueGetStudentYear:"+result)
  }
```

`C++`：

```C
aki::Value PassingValueGetStudentName(aki::Value value) {
    aki::Value result = value.CallMethod("getStudentName");
    return result; 
}

aki::Value PassingValueGetStudentYear(aki::Value value) {
    aki::Value result = value.CallMethod("getStudentYear");
    return result; 
}

JSBIND_GLOBAL()
{
    JSBIND_FUNCTION(PassingValueGetStudentName);
    JSBIND_FUNCTION(PassingValueGetStudentYear);
}
```


