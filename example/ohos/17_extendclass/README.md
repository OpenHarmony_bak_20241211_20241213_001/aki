## Example 17: *extendclass*

环境依赖：
* IDE： DevEco Studio 4.0 Beta2
* SDK：4.0.9.6
* IDE： DevEco Studio 3.1.0.500
* SDK：3.2.12.2

### 1. 依赖配置

- 本例使用源码依赖

    ```bash
    cd entry/src/main/cpp
    git clone
    ```

    CMakeLists.txt 配置依赖
    ```cmake
    add_subdirectory(aki)
    target_link_libraries(entry PUBLIC aki_jsbind)
    ```

### 2. 用例说明


`index.d.ts`:
```js

export class TaskRunnerData {
  name: string;
}

export class TaskRunner {
  DoTask: () => Promise<object>;
  DoTaskReturnData: () => Promise<TaskRunnerData>;
  DoTaskParamObjReturnStruct: (bean: TaskRunnerData) => Promise<TaskRunnerData>;
}

export const AsyncTaskReturnVoid: () => Promise<object>;

export const AsyncTaskLongLongFunctionReturnLong: (min: number, max: number, func: (a: number, b: number, c: string) => number) => Promise<number>;

export const AsyncTaskParmObjReturnStruct: (bean: TaskRunnerData) => Promise<TaskRunnerData>;

export class TaskRunnerChild {
  TaskRunnerChildReturnData: () => Promise<object>;
  DoTaskReturnData: () => Promise<TaskRunnerData>;
  DelayTask: () => Promise<number>;
}

export class TaskRunnerGrandson {
  TaskRunnerChildReturnData: () => Promise<object>;
  DoTaskReturnData: () => Promise<TaskRunnerData>;
  DelayTask: () => Promise<number>;
  TaskRunnerGrandsonReturnData: () => Promise<number>;
}
```

`index.ets`:
```js
  let taskRunnerChild = new libAddon.TaskRunnerChild();
            taskRunnerChild.TaskRunnerChildReturnData().then(res => {
              console.log('[AKI] taskRunnerChild TaskRunnerChildReturnData: ' + res)
            });
            taskRunnerChild.DoTaskReturnData().then(res => {
              console.log('[AKI] taskRunnerChild DoTaskReturnData: ' + res.name)
            });
            taskRunnerChild.DelayTask().then(res => {
              console.log('[AKI] taskRunnerChild DelayTask: ' + res)
            });
            let taskRunnerGrandson = new libAddon.TaskRunnerGrandson();

            taskRunnerGrandson.TaskRunnerGrandsonReturnData().then(res => {
              console.log('[AKI] TaskRunnerGrandson TaskRunnerGrandsonReturnData: ' + res)
            });

            taskRunnerGrandson.DelayTask().then(res => {
              console.log('[AKI] TaskRunnerGrandson DelayTask: ' + res)
            });

            taskRunnerGrandson.TaskRunnerChildReturnData().then(res => {
              console.log('[AKI] TaskRunnerGrandson TaskRunnerChildReturnData: ' + res)
            });

            taskRunnerGrandson.DoTaskReturnData().then(res => {
              console.log('[AKI] TaskRunnerGrandson DoTaskReturnData: ' + res.name)
           });

```

`C++`：

```C
struct TaskRunnerData {
    TaskRunnerData() = default;
    TaskRunnerData(std::string name) : name(name) {}
    std::string name;
};

JSBIND_CLASS(TaskRunnerData) {
    JSBIND_CONSTRUCTOR<>();
    JSBIND_PROPERTY(name);
}

class TaskRunner {
public:
    TaskRunner() = default;
    int DoTask() {
        AKI_LOG(INFO) << "DoTask";
        return 1;
    }
    void DoTaskReturnVoid() {
        return;
    }
    TaskRunnerData DoTaskReturnData() {
        AKI_LOG(INFO) << "DoTaskReturnData";
        return TaskRunnerData("aki");
    }
    
    TaskRunnerData DoTaskParamObjReturnStruct(TaskRunnerData obj) {
        AKI_LOG(INFO) << "DoTaskParamObjReturnStruct";
        return obj;
    }
};

class TaskRunnerTask {
    public:
    TaskRunnerTask() = default;
    int DelayTask() {
        AKI_LOG(INFO) << "DelayTask";
        return 1;
    }
    };

class TaskRunnerChild : public TaskRunner,public TaskRunnerTask {

public:
    TaskRunnerChild() = default;
    int TaskRunnerChildReturnData() {
        AKI_LOG(INFO) << "TaskRunnerChildReturnData";
        return 1;
    }
};

class TaskRunnerGrandson : public TaskRunnerChild {

public:
    TaskRunnerGrandson() = default;
    int TaskRunnerGrandsonReturnData() {
        AKI_LOG(INFO) << "TaskRunnerGrandsonReturnData";
        return 2;
    }
};

JSBIND_CLASS(TaskRunner) {
    JSBIND_CONSTRUCTOR<>();
    JSBIND_PMETHOD(DoTask);
    JSBIND_PMETHOD(DoTaskReturnVoid);
    JSBIND_PMETHOD(DoTaskReturnData);
    JSBIND_PMETHOD(DoTaskParamObjReturnStruct);
}

JSBIND_CLASS(TaskRunnerTask) {
    JSBIND_CONSTRUCTOR<>();
    JSBIND_PMETHOD(DelayTask);
}

JSBIND_CLASS(TaskRunnerChild) {
    JSBIND_EXTEND(TaskRunner);
    JSBIND_EXTEND(TaskRunnerTask);
    JSBIND_CONSTRUCTOR<>();
    JSBIND_PMETHOD(TaskRunnerChildReturnData);
}

JSBIND_CLASS(TaskRunnerGrandson) {
    JSBIND_EXTEND(TaskRunnerChild);
    JSBIND_CONSTRUCTOR<>();
    JSBIND_PMETHOD(TaskRunnerGrandsonReturnData);
}
```


