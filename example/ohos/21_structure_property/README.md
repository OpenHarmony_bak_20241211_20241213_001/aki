## Example 21: *structure_property*

环境依赖：
* IDE： DevEco Studio 4.0 Beta2
* SDK：4.0.9.6
* IDE： DevEco Studio 3.1.0.500
* SDK：3.2.12.2

### 1. 依赖配置

- 本例使用源码依赖

    ```bash
    cd entry/src/main/cpp
    git clone
    ```

  CMakeLists.txt 配置依赖
    ```cmake
    add_subdirectory(aki)
    target_link_libraries(entry PUBLIC aki_jsbind)
    ```

### 2. 用例说明

`index.d.ts`:

```js
export class Person{
    age: number;
    name: string;
    constructor(age: number)
}


export const foo: (bean: object) => Person;
```

`index.ets`:

```js
 Text("JSBIND_PROPERTY")
          .fontSize(50)
          .fontWeight(FontWeight.Bold)
          .onClick(() => {
            let person = new libAddon.Person(10);
            let other = libAddon.foo(person);
            hilog.info(0x0000, 'AKI', 'Person age = %{public}d,name= %{public}s',  other.age,other.name);
          })
```

`C++`:

```C
struct Person {
    // 构造函数，用于JS侧 new 对象
    Person(int age) : age(age) {}
    int age;
    std::string name;
};

JSBIND_CLASS(Person) {
    JSBIND_CONSTRUCTOR<int>(); // 绑定构造函数
    JSBIND_PROPERTY(age);      // 绑定成员属性
    JSBIND_PROPERTY(name);     // 绑定成员属性
}

Person foo(Person person) {
    person.age += 10;
    person.name = "Ark";
    return person;
}

JSBIND_GLOBAL() {
    JSBIND_FUNCTION(foo);
}
```