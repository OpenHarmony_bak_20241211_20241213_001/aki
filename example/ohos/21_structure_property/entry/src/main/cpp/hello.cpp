/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <string>

#include <aki/jsbind.h>

struct Person {
    // 构造函数，用于JS侧 new 对象
    Person(int age) : age(age) {}
    int age;
    std::string name;
};

JSBIND_CLASS(Person) {
    JSBIND_CONSTRUCTOR<int>(); // 绑定构造函数
    JSBIND_PROPERTY(age);      // 绑定成员属性
    JSBIND_PROPERTY(name);     // 绑定成员属性
}

Person foo(Person person)
{
    person.age += 10;
    person.name = "Ark";
    return person;
}

JSBIND_GLOBAL() {
    JSBIND_FUNCTION(foo);
}

JSBIND_ADDON(entry)