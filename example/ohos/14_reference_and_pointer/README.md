## Example 14: *reference_and_pointer*

环境依赖：
* IDE： DevEco Studio 4.0 Beta2
* SDK：4.0.9.6
* IDE： DevEco Studio 3.1.0.500
* SDK：3.2.12.2

### 1. 依赖配置

- 本例使用源码依赖

    ```bash
    cd entry/src/main/cpp
    git clone
    ```

    CMakeLists.txt 配置依赖
    ```cmake
    add_subdirectory(aki)
    target_link_libraries(entry PUBLIC aki_jsbind)
    ```

### 2. 用例说明


`index.d.ts`:
```js

export class Reference {
      count: number;
      constructor(count:number);
      ChangeByReference:(ref: Reference) => void;
      ChangeByPointer:(ref: Reference) => void;
      static MultiCountByReference:(ref: Reference,num:number) => void;
      static MultiCountByPointer:(ref: Reference,num:number) => void;
}

export const GrowUpByReference:(ref: Reference) => void;

export const GrowUpByPointer:(ref: Reference) => void;

```

`index.ets`:
```js
 let ref1 = new libAddon.Reference(0);
 libAddon.GrowUpByReference(ref1);
 hilog.info(0x0000, 'AKI', 'after GrowUpByReference ref1 count = %{public}d', ref1.count);

 let ref1 = new libAddon.Reference(0);
 libAddon.GrowUpByPointer(ref1);
 hilog.info(0x0000, 'AKI', 'after GrowUpByPointer ref1 count = %{public}d', ref1.count);
```

`C++`：

```C
namespace {
struct Reference {
    explicit Reference(int count) : count(count) {}
    
    static void MultiCountByReference(Reference& ref, int num)
    {
        ref.count *= num;
    }
    
    static void MultiCountByPointer(Reference* ptr, int num)
    {
        ptr->count *= num;
    }
    
    void ChangeByReference(Reference& ref)
    {
        int temp = ref.count;
        ref.count = this->count;
        this->count = temp;
    }
    
    void ChangeByPointer(Reference* ptr)
    {
        int temp = ptr->count;
        ptr->count = this->count;
        this->count = temp;
    }
    
    int count;
};
}

JSBIND_CLASS(Reference) {
    JSBIND_CONSTRUCTOR<int>(); // 绑定构造函数
    JSBIND_PROPERTY(count);    // 绑定成员属性
    JSBIND_METHOD(ChangeByReference);
    JSBIND_METHOD(ChangeByPointer);
    JSBIND_METHOD(MultiCountByReference);
    JSBIND_METHOD(MultiCountByPointer);
}

namespace {
void GrowUpByReference(Reference& ref)
{
    ref.count++;
}

void GrowUpByPointer(Reference* ptr)
{
    ptr->count++;
}
}

JSBIND_GLOBAL() {
    JSBIND_FUNCTION(GrowUpByReference);
    JSBIND_FUNCTION(GrowUpByPointer);
}
```


