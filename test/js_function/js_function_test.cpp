/*
 * Copyright (C) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <map>
#include <aki/jsbind.h>

std::string CallJSFunctionReturnVoid()
{
    std::string msg;
    if (auto func = aki::JSBind::GetJSFunction("foo")) {
        msg = func->Invoke<std::string>("msg from C++");
    }
    return msg;
}


bool  CallJSFunctionInvokeAsync()
{
    std::thread subThread([]() {
        if (auto func = aki::JSBind::GetJSFunction("helloAsync")) {
            std::function<void(std::string)> callback = [](std::string message) {
                AKI_DLOG(INFO) << "helloAsync callback will be invoked in JS Thread:"<<message;
            };
            func->InvokeAsync<void>("helloAsync hello world", callback);
            AKI_DLOG(INFO) << "helloAsync start.";
        }
    });
    subThread.detach();
    return true;
}

bool CallJSFunctionInvokeAsyncReturnValue() {
    std::thread subThread([]() {
        if (auto func = aki::JSBind::GetJSFunction("helloAsyncReturnValue")) {
            std::promise<std::string> promise;
            std::function<void(std::string)> callback = [&promise](std::string message) {
                AKI_DLOG(INFO) << "helloAsyncReturnValue callback will be invoked in JS Thread:" << message;
                promise.set_value(message);
            };
            auto result= func->InvokeAsync<std::string>("helloAsyncReturnValue hello world", callback);
            std::string returnValue= result.get();
            AKI_DLOG(INFO) << "helloAsyncReturnValue return value:" << returnValue;
            AKI_DLOG(INFO) << "helloAsyncReturnValue wait until promise set value.";
            std::string message = promise.get_future().get(); // 子线程阻塞
            AKI_DLOG(INFO) << "helloAsyncReturnValue promise with message: " << message;
        }
    });
    subThread.detach();
    return true;
}


JSBIND_GLOBAL() {
    JSBIND_FUNCTION(CallJSFunctionReturnVoid, "callJSFunctionReturnVoid");
    JSBIND_FUNCTION(CallJSFunctionInvokeAsync, "callJSFunctionInvokeAsync");
    JSBIND_FUNCTION(CallJSFunctionInvokeAsyncReturnValue, "callJSFunctionInvokeAsyncReturnValue");
}